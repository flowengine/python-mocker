# Python Mocker #

This is a simple python mocker intended for web develompent. You just configure the routes and the expected responses and focus on the development.

### Requirements ###

* Get Python 2.7.X
* [Get WebPy](http://webpy.org/)

### Fist steps ###
Set up the config.py file with your URLs and the expected behavour for each URL. Remember, the configured URLs must match the route of your API without the endpoint. Eg, if I have "http://my-api.com/resource/" I'd register "/resource" as URL. See example config.py for mor details.
After configuring your API, from command line execute:

```
#!bash

python webmock.py
```

For more information about parameters, such as port configuration or IP, check WebPy documentation.