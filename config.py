urls = (
	'/table', 'mocker',
	'/tables', 'mocker',
	'/dishes/mainCourse', 'mocker',
	'/dishes/snack', 'mocker',
	'/dishes/dessert', 'mocker',
	'/dishes/beverage', 'mocker',
)

route_config = {
	"/tables": {
		"filename" : "tables.json",
		"content_type" : "application/json"
	},
	"/table": {
		"filename" : "table.json",
		"content_type" : "application/json"
	},
	"/dishes/mainCourse": {
		"filename" : "mainCourses.json",
		"content_type" : "application/json"
	},
	"/dishes/snack": {
		"filename" : "snacks.json",
		"content_type" : "application/json"
	},
	"/dishes/dessert": {
		"filename" : "desserts.json",
		"content_type" : "application/json"
	},
	"/dishes/beverage": {
		"filename" : "beverages.json",
		"content_type" : "application/json"
	}

}
