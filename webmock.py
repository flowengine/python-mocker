import web
import json
import config
import os
import time

class JsonReader:
	def parse(self, nombre_archivo):
		nombre_archivo = os.path.join(os.path.dirname(__file__), nombre_archivo)
		with open(nombre_archivo, 'r') as archivo:
			jsonObject = json.load(archivo)
		return jsonObject


class mocker:
	jsonReader = JsonReader()
	def GET(self):
		time.sleep(2)
		return self.__mock_return(self.__content_type)

	def POST(self):
		return self.__mock_return(self.__content_type)

	def __mock_return(self, content_type):
		web.header('Content-Type', content_type)
		configuration = config.route_config[self.__current_path]
		jsonObject = self.jsonReader.parse(configuration["filename"])
		return json.dumps(jsonObject)

	@property
	def __content_type(self):
		return config.route_config[self.__current_path]["content_type"]

	@property
	def __current_path(self):
		return web.ctx.path

if __name__ == "__main__":
	web.application(config.urls, globals()).run()