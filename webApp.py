import web
import json

urls = (
    '/test', 'users',
    '/test/1', 'user'
)


class users:
    def GET(self):
        web.header('Content-Type', 'application/json')
	users = [ {"id": 1, "name":"Pepe"} , {"id":2, "name":"Florencia"} ]
	return json.dumps(users)

class user:
    def GET(self):
        web.header('Content-Type', 'application/json')
	user = {"id": 1, "name":"Pepe"}
	return json.dumps(user)

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
